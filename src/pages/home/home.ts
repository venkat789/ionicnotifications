import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { FCM } from '@ionic-native/fcm';
import { AngularFireDatabase } from 'angularfire2/database'
import { Push, PushObject, PushOptions } from '@ionic-native/push';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
    private fcm: FCM, private afDb: AngularFireDatabase,
    private toastController: ToastController,
    private push: Push
  ) {
    console.log('into constructor')

    fcm.subscribeToTopic('marketing');

    fcm.getToken().then(token => {
      console.log(token)
      localStorage.setItem('userToken', token)
      this.afDb.list('/userKeys').push({ name: 'Venkat', tokenVal: token })

    })
    const toast = this.toastController.create({
      message: 'User was added successfully',
      duration: 3000,
      position: 'top'
    });
    fcm.onNotification().subscribe(data => {
      const pushObject: PushObject = this.push.init(options);
      if (data.wasTapped) {
        console.log("Received in background" + data);
        pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));


      } else {
        if(typeof data.body !== 'undefined')
        console.log("Received in foreground" + data.body);
        else
          console.log("Received in foreground" + data);
        pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));

      };
    })
    // to check if we have permission
    this.push.hasPermission() 
      .then((res: any) => {
 
        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
        } else {
          console.log('We do not have permission to send push notifications');
        }

      });
    const options: PushOptions = {
      android: {},
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };


  }

}
