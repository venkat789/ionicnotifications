import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {AngularFireModule} from "angularfire2";

import {AngularFireDatabaseModule} from "angularfire2/database";
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FCM } from '@ionic-native/fcm';
import { Push} from '@ionic-native/push';


var config = {
    apiKey: "AIzaSyBK1WxuKF1zYVGBr6bE7ZnxwVOzmJi4K-Q",
    authDomain: "sample-ionic-app-725f4.firebaseapp.com",
    databaseURL: "https://sample-ionic-app-725f4.firebaseio.com",
    projectId: "sample-ionic-app-725f4",
    storageBucket: "sample-ionic-app-725f4.appspot.com",
    messagingSenderId: "595992876048"
  };

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ], 
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
       AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule, 
   
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FCM,
    Push
  ]
})
export class AppModule {}
